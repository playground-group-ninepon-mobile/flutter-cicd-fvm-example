FROM debian:latest

# update
RUN apt update -y
RUN apt upgrade -y

RUN mkdir -p /var/ninepon/flutter_cicd/

# install package
RUN apt -y install git curl unzip clang cmake ninja-build pkg-config libgtk-3-dev liblzma-dev openjdk-11-jdk
# RUN git clone https://github.com/flutter/flutter.git -b stable
# RUN export PATH="$PATH:`pwd`/flutter/bin"
RUN git clone https://github.com/flutter/flutter.git -b stable --depth 1 /usr/local/flutter
ENV PATH="/usr/local/flutter/bin:/usr/local/flutter/bin/cache/dart-sdk/bin:${PATH}"

# install JAVA
RUN apt -y install default-jdk
RUN export JAVA_HOME=/usr/lib/jvm/default-java
# RUN source .bashrc
RUN mkdir -p /home/flutter/Android/Sdk/
RUN export ANDROID_SDK_ROOT=/home/flutter/Android/Sdk/

# Set up Android SDK
RUn mkdir -p /usr/local/android-sdk/cmdline-tools
RUN curl -s https://dl.google.com/android/repository/commandlinetools-linux-7302050_latest.zip -o /tmp/tools.zip && \
    unzip /tmp/tools.zip -d /usr/local/android-sdk/cmdline-tools && \
    mv /usr/local/android-sdk/cmdline-tools/cmdline-tools /usr/local/android-sdk/cmdline-tools/latest && \
    rm /tmp/tools.zip
ENV ANDROID_SDK_ROOT=/usr/local/android-sdk
ENV PATH="${ANDROID_SDK_ROOT}/tools:${ANDROID_SDK_ROOT}/platform-tools:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:${PATH}"

# Accept licenses for Android SDK
RUN yes | sdkmanager --licenses

# sdkmanager
# RUN cd /usr/local/android-sdk/cmdline-toolsbin
RUN sdkmanager --sdk_root=/home/flutter/Android/Sdk/ "platform-tools" "build-tools;28.0.0" "platforms;android-28"

# RUN Flutter
RUN flutter

# run initial
RUN flutter clean | echo "flutter clean error"
# RUN flutter pub get
# RUN flutter build appbundle

# install node and package
RUN apt install nodejs npm -y
RUN npm install -g publish-aab-google-play

CMD ["bash"]